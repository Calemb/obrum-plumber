﻿using UnityEngine;

public class BridgeMaintainer : MonoBehaviour
{
    [SerializeField] private Transform BridgeSlot;
    [SerializeField] private float foldAngle = 120f;
    [SerializeField] private float spreadAngle = 0f;

    private Bridge bridge;
    private bool bridgeAttached = false;
    private bool isControllActive;
    private bool bridgeMaintenance = false;

    public void SetAllowUse(bool enable)
    {
        isControllActive = enable;
    }

    public bool IsBridgeUnderMaintenance()
    {
        return bridge == null ? false : bridgeMaintenance;
    }

    public void BridgeInRangeChange(Bridge newBrdige)
    {
        bridge = newBrdige;
    }

    private void Update()
    {

        //https://www.lowlypoly.com        ///
        if (isControllActive &&
         !IsBridgeUnderMaintenance() &&
         bridge != null &&
         Input.GetKeyDown(KeyCode.E)
         )
        {
            if (bridgeAttached)
            {
                StartSpreading();
            }
            else
            {
                StartFolding();
            }

            bridgeAttached = !bridgeAttached;
        }
    }

    private void StartFolding()
    {
        bridgeMaintenance = true;

        bridge.SetColliders(false);
        bridge.EnableNavMeshSourceTag(false);

        bridge.AttachToParent(BridgeSlot);

        BridgeSlot.localEulerAngles = new Vector3(foldAngle, 0, 0);
        bridge.SetFoldAngle();
    }

    private void StartSpreading()
    {
        bridgeMaintenance = true;
        bridge.SetSpreadAngle();
        BridgeSlot.localEulerAngles = new Vector3(spreadAngle, 0, 0);

        bridge.DetachFromParent();
        bridge.EnableNavMeshSourceTag(true);
        bridge.SetColliders(true);
    }

    private void EndMaintenance()
    {
        bridgeMaintenance = false;
    }
}
