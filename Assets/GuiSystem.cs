﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GuiSystem : MonoBehaviour
{
    [Header("Gameplay")]
    [SerializeField] private GameObject gameplayPanel;

    [SerializeField] private Text stageTimer;
    [SerializeField] private Text cyvilsOnMapText;
    [SerializeField] private Text cyvilsSavedText;

    [Header("EndGame")]
    [SerializeField] private GameObject endPanel;
    [SerializeField] private Text stageNameText;
    [SerializeField] private Text gameResultText;
    [SerializeField] private Button restartGameButton;

    [Header("Pause")]
    [SerializeField] private PauseMenu pauseMenu;

    private void Awake()
    {
        endPanel.SetActive(false);

        restartGameButton.onClick.AddListener(OnRestartButtonClicked);
    }

    public void SetCyvilsOnMap(int amount)
    {
        cyvilsOnMapText.text = amount.ToString();
    }

    public void SetCyvilsSaved(int amount)
    {
        cyvilsSavedText.text = amount.ToString();
    }
    public void SetTimer(string timer)
    {
        stageTimer.text = timer;
    }

    public void SetStageName(string name)
    {
        stageNameText.text = name;
    }

    public void SetGameWin()
    {
        endPanel.SetActive(true);
        gameplayPanel.SetActive(false);
        gameResultText.text = "WIN!";
    }

    public void SetGameLose()
    {
        endPanel.SetActive(true);
        gameplayPanel.SetActive(false);
        gameResultText.text = "LOSE!";
    }

    public void OnRestartButtonClicked()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
}
