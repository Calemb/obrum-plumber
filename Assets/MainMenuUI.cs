﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuUI : MonoBehaviour
{
    [SerializeField] private GameObject MainMenuPanel;
    [SerializeField] private GameObject OptionsPanel;

    private void Start()
    {
        HideOptionsPanel();
    }

    private void HideOptionsPanel()
    {
        OptionsPanel.SetActive(false);
    }

    public void OnStartLevelClicked()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    public void OnOptionsMenuClicked()
    {
        ShowOptionsPanel(true);
        ShowMainMenu(false);
    }

    private void ShowMainMenu(bool visible)
    {
        MainMenuPanel.SetActive(visible);
    }

    private void ShowOptionsPanel(bool visible)
    {
        OptionsPanel.SetActive(visible);
    }

    public void OnExitButtonClicked()
    {
        Application.Quit();
    }


    public void OnBackFromOptionsButtonClicked()
    {
        ShowMainMenu(true);
        ShowOptionsPanel(false);
    }
}

