﻿using System;
using UnityEngine;
using UnityEngine.AI;

public class Cyvil : MonoBehaviour
{
    private Action<Cyvil> onDieActions;
    private NavMeshAgent agent;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.Warp(transform.position);
        agent.SetDestination(transform.position);
    }

    public void SetExitPosition(Vector3 exitPos)
    {
        agent.updatePosition = true;
        agent.updateRotation = true;
        agent.SetDestination(exitPos);
    }

    public void RegisterOnExitCallback(Action<Cyvil> action)
    {
        onDieActions += action;
    }

    public void ExitReached()
    {
        onDieActions.Invoke(this);
        Destroy(gameObject);
    }
}
