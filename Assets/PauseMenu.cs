﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private GameObject window;

    private void Awake()
    {
        window.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePauseMenu();
        }
    }

    private bool isMenuActive()
    {
        return window.activeInHierarchy;
    }

    public void OnBackMainMenuButtonClicked()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }

    public void OnResumeButtonClicked()
    {
        TogglePauseMenu();
    }

    private void TogglePauseMenu()
    {
        if (isMenuActive())
        {
            HideMenu();
        }
        else
        {
            ShowMenu();
        }

        Time.timeScale = isMenuActive() ? 0 : 1;
    }

    private void ShowMenu()
    {
        window.SetActive(true);
    }

    private void HideMenu()
    {
        window.SetActive(false);
    }

    private void OnDestroy()
    {
        Time.timeScale = 1;
    }
}