﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BridgeDetector : MonoBehaviour
{
    private LineRenderer lineRenderer;

    private List<BridgeIndicator> indicatorsInRange = new List<BridgeIndicator>();

    private Bridge bridgeInRange = null;

    private Action<Bridge> onBridgeInRangeAction;

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
        SetLineRendererColor(Color.red);
    }

    public void OnBridgeInRange(Action<Bridge> a)
    {
        onBridgeInRangeAction += a;
    }
    public void OffBridgeInRange(Action<Bridge> a)
    {
        onBridgeInRangeAction -= a;
    }

    private void OnTriggerEnter(Collider other)
    {
        //czy to w ogóle indicator
        if (other.CompareTag("BridgeIndicator"))
        {
            BridgeIndicator bi = other.GetComponent<BridgeIndicator>();

            if (indicatorsInRange.Any(x => x.GetBridge() == bi.GetBridge()))
            {
                //we have a bridge edge!
                bridgeInRange = bi.GetBridge();
                onBridgeInRangeAction(bridgeInRange);
                SetLineRendererColor(Color.green);

                Debug.Log("Just catch bridge!");
            }

            //=> assume that new one arrive only after trigger exit -> so there should be always uniq list
            indicatorsInRange.Add(bi);
            Debug.Log("Added indicator: " + bi.name);
        }
    }

    internal void TurnIndicator(bool enabled)
    {
        lineRenderer.enabled = enabled;
    }

    private void SetLineRendererColor(Color newColor)
    {
        List<Material> materials = new List<Material>();
        lineRenderer.GetSharedMaterials(materials);
        materials.First().color = newColor;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("BridgeIndicator"))
        {
            BridgeIndicator bi = other.GetComponent<BridgeIndicator>();
            indicatorsInRange.Remove(bi);
            Debug.Log("Removed indicator: " + bi.name);

            //so there is another indicator from same bridge
            //which means - we just 'split' pair of bridge indicators
            //so there is no bridge in range

            //there is no chance so we have two indicators from opposite side of bridge!
            if (indicatorsInRange.Any(x => x.GetBridge() == bi.GetBridge()))
            {
                bridgeInRange = null;
                onBridgeInRangeAction(null);
                SetLineRendererColor(Color.red);
            }
        }
    }
}
