﻿using UnityEngine;

public class BridgeIndicator : MonoBehaviour
{
    [SerializeField] private Bridge bridge;

    public Bridge GetBridge()
    {
        return bridge;
    }
}
