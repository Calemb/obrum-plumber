﻿using System;
using UnityEngine;

public class DistanceBridgeDetector : MonoBehaviour
{
    private Action<BridgeIndicator> onBridgeInLongRangeAction;

    public void OnBridgeInRange(Action<BridgeIndicator> a)
    {
        onBridgeInLongRangeAction += a;
    }
    public void OffBridgeInRange(Action<BridgeIndicator> a)
    {
        onBridgeInLongRangeAction -= a;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("BridgeIndicator"))
        {
            onBridgeInLongRangeAction(other.GetComponent<BridgeIndicator>());
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("BridgeIndicator"))
        {
            onBridgeInLongRangeAction(null);
        }
    }
}
