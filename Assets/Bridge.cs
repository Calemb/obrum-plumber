﻿using UnityEngine;

public class Bridge : MonoBehaviour
{
    [SerializeField] private ControllerAnimation animController;
    [SerializeField] private Rigidbody rb;
    [SerializeField] private Transform rotateTransform;
    [SerializeField] private float foldAngle = 0f;
    [SerializeField] private float spreadAngle = 180f;

    [SerializeField] private Collider colliderOne;
    [SerializeField] private Collider colliderTwo;

    [SerializeField] private NavMeshSourceTag partOne;
    [SerializeField] private NavMeshSourceTag partTwo;

    private bool animLocked = false;
    private Vector3 bridgeInitPos;
    private Quaternion bridgeInitRot;

    private void Awake()
    {
        //bridgeInitPos = transform.localPosition;
        //bridgeInitRot = transform.localRotation;

        //EnableNavMeshSourceTag(false);
        //SetColliders(false);
    }

    public void EnableNavMeshSourceTag(bool enabled)
    {
        partOne.enabled = enabled;
        partTwo.enabled = enabled;
    }

    public void SetColliders(bool active)
    {
        colliderOne.enabled = active;
        colliderTwo.enabled = active;
    }

    public void AttachToParent(Transform newParent)
    {
        //animLocked = true;

        //rb.isKinematic = true;
        //SetColliders(false);
        //EnableNavMeshSourceTag(false);

        transform.SetParent(newParent, true);
        //transform.localPosition = bridgeInitPos;
        //transform.localRotation = bridgeInitRot;

        //animController.StartFold();
    }

    public void DetachFromParent()
    {
        //animLocked = true;
        transform.SetParent(null, true);

        //rb.isKinematic = false;
        //SetColliders(true);
        //EnableNavMeshSourceTag(true);

        //animController.StartSpreading();

    }

    public void SetFoldAngle()
    {
        rotateTransform.localEulerAngles = new Vector3(foldAngle, 180, 0);
    }

    public void SetSpreadAngle()
    {
        rotateTransform.localEulerAngles = new Vector3(spreadAngle, 180, 0);
    }

    public bool isAnimLocked()
    {
        return animLocked;
    }

    //public void OnAnim_BridgeFolded()
    //{
    //    Debug.Log("FOlded!");
    //    animLocked = false;
    //}

    //public void OnAnim_BridgeSpreaded()
    //{
    //    Debug.Log("Spreaded!");
    //    animLocked = false;
    //    transform.SetParent(null, true);

    //    //rb.isKinematic = false;
    //    SetColliders(true);
    //    EnableNavMeshSourceTag(true);
    //}
}
