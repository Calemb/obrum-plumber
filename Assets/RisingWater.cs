﻿using System.Collections;
using UnityEngine;

public class RisingWater : MonoBehaviour
{
    [SerializeField] private float targetHeight = 5.0f;

    public void RiseWater(float duringTime)
    {
        StartCoroutine(RiseWaterCo(duringTime));
    }

    private IEnumerator RiseWaterCo(float duringTime)
    {
        float distance = Mathf.Abs(transform.position.y - targetHeight);
        float step = distance / duringTime;

        Vector3 newPos = transform.position;

        while (transform.position.y < targetHeight)
        {
            newPos.y += Time.deltaTime * step;
            transform.position = newPos;

            yield return null;
        }
    }
}
