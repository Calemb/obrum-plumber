﻿using UnityEngine;

public class GameCamera : MonoBehaviour
{
    private float _colliderLocation = 1;//0-1
    private const float _colliderOffsetNear = 0.01f;                     //< Collider needs to be a little in front of the near clip plane to work properly
    [SerializeField] private BoxCollider traceableCollider;

    // Start is called before the first frame update
    private void Start()
    {
        //FitColliderToFrustum(traceableCollider, Camera.main);
    }

    // Update is called once per frame
    private void Update()
    {
        //FitColliderToFrustum(traceableCollider, Camera.main);
    }

    private void FitColliderToFrustum(BoxCollider collider, Camera camera)
    {
        if (collider == null || camera == null)
        {
            return;
        }

        // ColliderLocation need to be in [0..1] range
        float positionFactor = Mathf.Clamp01(_colliderLocation);
        // How much in front of the camera must be collider. Front face of the collider must be a bit in front of the near clip plane.
        float depth = Mathf.Lerp(camera.nearClipPlane + _colliderOffsetNear, camera.farClipPlane, positionFactor);
        float height = Mathf.Tan(camera.fieldOfView * Mathf.Deg2Rad * 0.5f) * depth * 2.0f;
        float width = camera.aspect * height;

        Vector3 size = collider.size;

        // Offset the collider, so that the front face is at the correct location (not the center of the collider).
        depth += size.z * 0.5f;

        // Update collider position and size
        collider.transform.localPosition = new Vector3(0.0f, 0.0f, depth);
        size = new Vector3(width, height, size.z);
        collider.size = size;
    }
}
