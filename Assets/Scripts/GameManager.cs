﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    //timery niezalezne - same licza
    //timery licza w dol!
    public GameObject agentTarget;
    [SerializeField] private SpawnSystem spawnSystem;
    [SerializeField] private GuiSystem gui;
    [SerializeField] private RisingWater risingWater;

    private RoundTimer cyvilTimer;
    private RoundTimer floodTimer;
    private RoundTimer floodKillTimer;

    [SerializeField] private float roundTimeTillCyvilsRunOut = 5.0f;
    [SerializeField] private float roundTimeTillFlood = 995.0f;//should start ticking after cywils run out
    [SerializeField] private float timeBeforeFloodKillsCyvils = 995.0f;

    private void Start()
    {
        cyvilTimer = new RoundTimer();
        floodTimer = new RoundTimer();
        floodKillTimer = new RoundTimer();

        cyvilTimer.Start();
        gui.SetStageName("PREPARE");
    }

    private void Update()
    {
        cyvilTimer.Update(Time.deltaTime);
        floodTimer.Update(Time.deltaTime);
        floodKillTimer.Update(Time.deltaTime);

        if (cyvilTimer.isTicking())
        {
            gui.SetTimer((roundTimeTillCyvilsRunOut - cyvilTimer.GetTime()).ToString());

            if (cyvilTimer.GetTime() >= roundTimeTillCyvilsRunOut)
            {
                Debug.Log("Start cyvilians!!");
                spawnSystem.InitSpawns();

                cyvilTimer.Stop();

                floodTimer.Start();
                gui.SetStageName("People are coming!");
            }
        }


        if (floodTimer.isTicking())
        {
            gui.SetTimer((roundTimeTillFlood - floodTimer.GetTime()).ToString());

            if (floodTimer.GetTime() >= roundTimeTillFlood)
            {
                Debug.Log("Start flooding");
                floodTimer.Stop();

                floodKillTimer.Start();

                gui.SetStageName("Flood is coming!!!!");
                risingWater.RiseWater(timeBeforeFloodKillsCyvils);
            }
        }

        if (floodKillTimer.isTicking())
        {

            gui.SetTimer((timeBeforeFloodKillsCyvils - floodKillTimer.GetTime()).ToString());

            if (floodKillTimer.GetTime() >= timeBeforeFloodKillsCyvils)
            {
                floodKillTimer.Stop();
                Time.timeScale = 0;
                gui.SetGameLose();
            }
        }
    }
}

