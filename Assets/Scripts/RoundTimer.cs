﻿using System;

public class RoundTimer
{
    private float timer = 0.0f;
    private bool ticking = false;

    public void Start()
    {
        ticking = true;
    }

    public void Update(float deltaTime)
    {
        if (ticking)
        {
            timer += deltaTime;
        }
    }

    public void Stop()
    {
        ticking = false;
    }

    public bool isTicking()
    {
        return ticking;
    }

    public float GetTime()
    {
        return timer;
    }
}
