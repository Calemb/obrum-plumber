﻿using UnityEngine;


public class ControlManager : MonoBehaviour
{
    [SerializeField] private GameCameraMovement camController;
    private DriveController activeDriveController = null;
    public static ControlManager Instance;

    private void Awake()
    {
        //TODO fuck this singleton - use injection or make it valid one!
        Instance = this;
    }




    // Start is called before the first frame update
    private void Start()
    {
        camController.ActivateControll();
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            if (activeDriveController != null)
            {
                //TODO also check if activedriver is not during crossing sequence
                activeDriveController.DisableControll();
                activeDriveController = null;

                camController.ActivateControll();
            }
        }
    }

    public void AttemptToControll(DriveController driveController)
    {
        if (activeDriveController == null)
        {
            activeDriveController = driveController;
            activeDriveController.ActivateControll();

            camController.DisableControll();
        }
    }
}
