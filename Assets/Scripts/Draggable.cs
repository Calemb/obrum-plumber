﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Draggable : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private bool dragging = false;
    public float _dragSpeed = 0.09f;
    private float dist = 0;

    private void Awake()
    {
        dist = Camera.main.GetComponent<Camera>().farClipPlane + 1000;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("Begin drag");
        DragObject(eventData);
    }

    public void OnDrag(PointerEventData eventData)
    {
        DragObject(eventData);
    }

    private void DragObject(PointerEventData eventData)
    {
        //MoveByRaytrace();

        //MoveByDelta(eventData);
    }

    private void MoveByRaytrace()
    {
        Vector3 pointOfGrabObject;
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        int layer = 1 << 9;

        //if (Physics.Raycast(ray, out hit, dist))
        Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.position + ray.direction * dist);
        if (Physics.Raycast(ray, out hit, dist, layer))
        {
            Debug.Log("HIT");
            Debug.Log(hit.collider.gameObject.name);
            Vector3 hittedObjPos = hit.collider.transform.position;

            pointOfGrabObject = hit.point;
            pointOfGrabObject.y = transform.position.y;

            gameObject.transform.position = pointOfGrabObject;
            //Vector3 relativehitpt = rayposobj - pointOfGrabObject;
        }
    }

    private void MoveByDelta(PointerEventData eventData)
    {
        Vector3 direction = new Vector3(eventData.delta.x, 0, eventData.delta.y);
        Vector3 move = direction * Time.deltaTime * _dragSpeed * Screen.dpi * 0.01f;

        move = Camera.main.transform.TransformVector(move);
        move.y = 0;
        ///

        transform.Translate(move, Space.World);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("on end drag");
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        //throw new System.NotImplementedException();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        //throw new System.NotImplementedException();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //throw new System.NotImplementedException();
    }

    //public void OnPointerDown(PointerEventData eventData)
    //{
    //    Debug.Log("pointer down");
    //    dragging = true;
    //}

    //public void OnPointerUp(PointerEventData eventData)
    //{
    //    dragging = false;
    //}

    //private void Update()
    //{
    //    if (dragging)
    //    {
    //        gameObject.transform.position = Event.current.delta;
    //    }
    //}
}
