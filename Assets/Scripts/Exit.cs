﻿using UnityEngine;

public class Exit : MonoBehaviour
{
    // Start is called before the first frame update
    private void Start()
    {

    }

    // Update is called once per frame
    private void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Cyvil")
        {
            Cyvil c = other.gameObject.GetComponent<Cyvil>();
            c.ExitReached();
        }
    }
}
