﻿using UnityEngine;
using UnityEngine.EventSystems;

//click to start controll
public class DriveController : BaseController, IControllable, IPointerClickHandler
{
    [SerializeField] private float driveSpeed = 5.0f;
    [SerializeField] private float rotateSpeed = 5.0f;

    [SerializeField] private Rigidbody rb;
    [SerializeField] private ForceMode forceMode = ForceMode.Acceleration;

    [SerializeField] private BridgeDetector bridgeDetector;
    [SerializeField] private DistanceBridgeDetector distanceBridgeDetector;

    private float driveSpeedFactor = 1.0f;
    [SerializeField] private BridgeMaintainer bridgeMaintainer;
    [SerializeField] private GameObject selection;

    private void Start()
    {
        bridgeDetector.TurnIndicator(false);

        distanceBridgeDetector.OnBridgeInRange(bridgeInLongRange);
        bridgeDetector.OnBridgeInRange(bridgeMaintainer.BridgeInRangeChange);

        TurnSelectionOff();
    }

    private void bridgeInLongRange(BridgeIndicator obj)
    {
        if (obj == null)
        {
            bridgeDetector.TurnIndicator(false);
        }
        else
        {
            bridgeDetector.TurnIndicator(true);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        ControlManager.Instance.AttemptToControll(this);

        //TODO cam go to follow state and animate to certain point
        //TODO after release cam stays but is free now (add some effect later)
    }


    private void FixedUpdate()
    {
        if (CanSteerVehicle())
        {
            float h = Input.GetAxis("Horizontal");
            float v = Input.GetAxis("Vertical");

            //Debug.Log("h " + h + "::" + v + "v");
            //Debug.Log(rb.velocity.magnitude);
            if (rb.velocity.magnitude > 3.0f)
            {
                Quaternion deltaRotation = Quaternion.Euler(new Vector3(0, h * Mathf.Sign(v), 0) * Time.deltaTime * rotateSpeed);
                rb.MoveRotation(rb.rotation * deltaRotation);
            }

            Vector3 move = new Vector3(0, 0, v);
            //disable cam steer
            move = move * Time.deltaTime * driveSpeed * driveSpeedFactor;

            rb.AddRelativeForce(move, forceMode);


            //zmien origin rotacji MS'a i zrob zeby sie krecil tylko gdy dodajemy force naprzod

            //if (h != 0 || v != 0)
            //{
            //    driveSpeedFactor -= Time.deltaTime;
            //    driveSpeedFactor = Mathf.Clamp(driveSpeedFactor, 1.0f, 1.0f);
            //}
            //else
            //{
            //    driveSpeedFactor = 1.0f;
            //}
        }
    }

    private bool CanSteerVehicle()
    {
        return isControllActive && !bridgeMaintainer.IsBridgeUnderMaintenance();
    }

    public void ActivateControll()
    {
        isControllActive = true;
        bridgeMaintainer.SetAllowUse(true);

        TurnSelectionOn();
    }

    private void TurnSelectionOn()
    {
        selection.SetActive(true);
    }

    public void DisableControll()
    {
        bridgeMaintainer.SetAllowUse(false);
        isControllActive = false;

        TurnSelectionOff();
    }

    private void TurnSelectionOff()
    {
        selection.SetActive(false);
    }
}