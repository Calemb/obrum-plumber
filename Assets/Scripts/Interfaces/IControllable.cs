﻿public interface IControllable
{
    void ActivateControll();
    void DisableControll();
}
