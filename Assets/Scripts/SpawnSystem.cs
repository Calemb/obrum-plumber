﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSystem : MonoBehaviour
{
    [SerializeField] private GameObject inputGameObject;
    [SerializeField] private GameObject exitGameObject;
    [SerializeField] private GameObject CyvilPrefab;
    [SerializeField] private GuiSystem gui;
    private List<Cyvil> allCyvils = new List<Cyvil>();

    [SerializeField] private int cyvilsToSpawn = 5;

    private int savedCyvils = 0;

    private void Start()
    {

        gui.SetCyvilsSaved(savedCyvils);

        gui.SetCyvilsOnMap(allCyvils.Count);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            allCyvils.ForEach(agent =>
            {
                agent.SetExitPosition(exitGameObject.transform.position);
            });
        }
    }
    public void Spawn()
    {
        GameObject cyvil = Instantiate(CyvilPrefab,
            inputGameObject.transform.position + Random.insideUnitSphere,
            Quaternion.identity,
            inputGameObject.transform);

        Cyvil c = cyvil.GetComponent<Cyvil>();
        c.RegisterOnExitCallback(CyvilExited);

        allCyvils.Add(c);

        gui.SetCyvilsOnMap(allCyvils.Count);
    }

    private void CyvilExited(Cyvil c)
    {
        allCyvils.Remove(c);
        savedCyvils += 1;
        gui.SetCyvilsSaved(savedCyvils);

        gui.SetCyvilsOnMap(allCyvils.Count);

        //if all cyvils gone -> WIN!
        if (allCyvils.Count == 0)
        {
            Time.timeScale = 0;
            gui.SetGameWin();
        }
    }

    public void InitSpawns()
    {
        StartCoroutine(CyclicSpawning(cyvilsToSpawn));
    }

    private IEnumerator CyclicSpawning(int cyvilsToSpawn)
    {
        int currentSpawns = 0;
        while (currentSpawns < cyvilsToSpawn)
        {
            Spawn();
            currentSpawns++;
            yield return new WaitForSeconds(1);
        }
    }
}