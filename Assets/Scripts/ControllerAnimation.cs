﻿using UnityEngine;

public class ControllerAnimation : MonoBehaviour
{
    [SerializeField] private Animator animator;

    public void StartSpreading()
    {
        animator.SetTrigger("Spread");
    }

    public void StartFold()
    {
        animator.SetTrigger("Fold");
    }

}