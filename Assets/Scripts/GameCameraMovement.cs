﻿using UnityEngine;

public class GameCameraMovement : BaseController, IControllable
{
    private Vector3 moveVector = Vector3.zero;
    private Camera gameCamera;
    private Vector3 lastMousePos = Vector3.zero;
    [SerializeField] private float rotSpeed = 1;
    [SerializeField] private float moveSpeed = 1;
    [SerializeField] private float zoomSpeed = 1;

    public void ActivateControll()
    {
        isControllActive = true;
    }
    public void DisableControll()
    {
        isControllActive = false;

    }

    // Start is called before the first frame update
    private void Awake()
    {
        gameCamera = Camera.main;
    }

    // Update is called once per frame
    private void Update()
    {
        Steering();
    }

    private void Steering()
    {
        if (isControllActive)
        {
            //move
            float h = Input.GetAxis("Horizontal");
            float v = Input.GetAxis("Vertical");

            float vertUp = Input.GetKey(KeyCode.E) ? 1 : 0;
            float vertDown = Input.GetKey(KeyCode.Q) ? 1 : 0;
            float vert = vertUp - vertDown;
            vert = vert * Time.deltaTime;

            moveVector.x = h;
            moveVector.z = v;

            float zoom = Input.GetAxis("Mouse ScrollWheel");
            zoom *= -1 * zoomSpeed;

            gameCamera.transform.Translate(moveVector * moveSpeed * Time.deltaTime);
            gameCamera.fieldOfView += zoom;

            gameCamera.transform.position = gameCamera.transform.position + new Vector3(0, vert, 0) * moveSpeed;

            //rotate
            if (Input.GetMouseButton(1))
            {
                if (lastMousePos == Vector3.zero)
                {
                    lastMousePos = Input.mousePosition;
                }

                Vector3 delta = Input.mousePosition - lastMousePos;

                lastMousePos = Input.mousePosition;
                delta *= rotSpeed * Time.deltaTime;
                float rotX = delta.y * -1;
                float rotY = delta.x;

                Camera.main.transform.Rotate(rotX, 0, 0, Space.Self);
                Camera.main.transform.Rotate(0, rotY, 0, Space.World);
            }
            if (Input.GetMouseButtonUp(1))
            {
                lastMousePos = Vector3.zero;
            }
        }
    }
}
